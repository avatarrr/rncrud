import React, {createContext, useReducer} from 'react';
import users from '../data/users';

const UsersContext = createContext({});
const initialState = {users};

const actions = {
  createUser(state, action) {
    const user = action.payload;
    user.id = Math.random();
    return {...state, users: [...state.users, user]};
  },
  deleteUser(state, action) {
    const user = action.payload;
    return {...state, users: state.users.filter(u => u.id !== user.id)};
  },
  updateUser(state, action) {
    const user = action.payload;
    return {
      ...state,
      users: state.users.map(u => (u === user.id ? user : u)),
    };
  },
};

export const UsersProvider = props => {
  const reducer = (state, action) => {
    const task = actions[action.type];

    return task ? task(state, action) : state;
  };

  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <UsersContext.Provider value={{state, dispatch}}>
      {props.children}
    </UsersContext.Provider>
  );
};

export default UsersContext;
