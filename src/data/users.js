export default [
  {
    id: 1,
    name: 'João Silva',
    email: 'joao_silva@rncrud.com',
    avatarUrl:
      'https://cdn.pixabay.com/photo/2017/01/31/21/23/avatar-2027366_960_720.png',
  },
  {
    id: 2,
    name: 'Marina Joaquina',
    email: 'marina_joaquina@rncrud.com',
    avatarUrl:
      'https://cdn.pixabay.com/photo/2022/08/19/19/29/anime-7397617_960_720.png',
  },
  {
    id: 3,
    name: 'Adalberto Romero',
    email: 'adalberto_romero@rncrud.com',
    avatarUrl:
      'https://cdn.pixabay.com/photo/2016/04/01/10/04/amusing-1299756_960_720.png',
  },
  {
    id: 4,
    name: 'Vinícius da Conceição',
    email: 'vinicius_conceicao@rncrud.com',
    avatarUrl:
      'https://cdn.pixabay.com/photo/2016/04/01/10/04/amusing-1299757_960_720.png',
  },
];
