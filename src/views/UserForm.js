import {Button, Text} from '@rneui/base';
import React, {useContext, useState} from 'react';
import {SafeAreaView, StyleSheet, TextInput} from 'react-native';
import UsersContext from '../context/UsersContext';

export default props => {
  const [user, setUser] = useState(
    props.route.params ? props.route.params : {},
  );

  const {dispatch} = useContext(UsersContext);

  return (
    <SafeAreaView style={styles.form}>
      <>
        <Text style={styles.text}>Name</Text>
        <TextInput
          style={styles.input}
          onChangeText={name => setUser({...user, name})}
          placeholder="Put a name!"
          value={user.name}
        />
      </>
      <>
        <Text style={styles.text}>Email</Text>
        <TextInput
          style={styles.input}
          onChangeText={email => setUser({...user, email})}
          placeholder="Put a email!"
          value={user.email}
        />
      </>
      <>
        <Text style={styles.text}>Avatar URL</Text>
        <TextInput
          style={styles.input}
          onChangeText={avatarUrl => setUser({...user, avatarUrl})}
          placeholder="Put a URL of an image!"
          value={user.avatarUrl}
        />
      </>
      <SafeAreaView style={styles.buttonView}>
        <Button
          title="Save"
          onPress={() => {
            dispatch({
              type: user.id ? 'updateUser' : 'createUser',
              payload: user,
            });
            props.navigation.goBack();
          }}
        />
      </SafeAreaView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  form: {
    flex: 1,
    padding: 12,
  },
  buttonView: {flex: 1, justifyContent: 'flex-end'},
  text: {
    paddingBottom: 5,
  },
  input: {
    height: 40,
    borderColor: 'grey',
    borderWidth: 1,
    marginBottom: 15,
    borderRadius: 10,
    paddingHorizontal: 10,
  },
});
