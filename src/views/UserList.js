import {Avatar, Button, ListItem} from '@rneui/base';
import React, {useContext} from 'react';
import {Alert, FlatList, SafeAreaView, StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import UsersContext from '../context/UsersContext';

export default props => {
  const {state, dispatch} = useContext(UsersContext);

  const editItemButton = edit => {
    return (
      <Button
        title="Edit"
        onPress={() => edit()}
        icon={{name: 'edit', color: 'white'}}
        buttonStyle={styles.button}
      />
    );
  };

  const deleteItemButton = edit => {
    return (
      <Button
        color="red"
        title="Remove"
        onPress={() => edit()}
        icon={{name: 'delete', color: 'white'}}
        buttonStyle={styles.button}
      />
    );
  };

  const getUserItem = ({item: user}) => {
    return (
      <ListItem.Swipeable
        linearGradientProps={{
          colors: ['#FF9800', '#F44336'],
          start: {x: 1, y: 0},
          end: {x: 0.2, y: 0},
        }}
        ViewComponent={LinearGradient}
        leftContent={editItemButton(() =>
          props.navigation.navigate('UserForm', user),
        )}
        rightContent={deleteItemButton(() => {
          Alert.alert('Are you sure?', 'This not be undone!', [
            {
              text: 'Yes',
              onPress: () => {
                dispatch({type: 'deleteUser', payload: user});
              },
            },
            {text: 'No'},
          ]);
        })}>
        <Avatar rounded title={user.name} source={{uri: user.avatarUrl}} />
        <ListItem.Content>
          <ListItem.Title style={styles.listItemTitle}>
            {user.name}
          </ListItem.Title>
          <ListItem.Subtitle style={styles.listItemSubtitle}>
            {user.email}
          </ListItem.Subtitle>
        </ListItem.Content>
      </ListItem.Swipeable>
    );
  };

  return (
    <SafeAreaView>
      <FlatList
        data={state.users}
        keyExtractor={user => user.id.toString()}
        renderItem={getUserItem}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  listItemTitle: {color: 'white', fontWeight: 'bold'},
  listItemSubtitle: {color: 'white'},
  button: {minHeight: '100%'},
});
